module.exports = {
  corePlugins: {
    textDecoration: false,
  },
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        body: ['Source Sans Pro']
      },
      backgroundImage: theme => ({
        'left-header': "url('assets/images/header/bg_left.svg')",
        'right-header': "url('assets/images/header/bg_right.svg')",
        'video-bg': "url('assets/images/video.svg')",
      }),
      backgroundColor: theme => ({
        'secondary': 'rgba(252, 164, 164, 0.09)',
        'button': '#3F3F3F'
      }),
      textColor: {
        'title': '#F72828',
        'value-text': '#FCA4A4'
      },
      width: {
        'box': '215px',
      },
      height: {
        'box': '215px',
      }
    },
  },
  variants: {
    extend: {
      borderColor: ['focus', 'hover'],
    },
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
  ],
}
